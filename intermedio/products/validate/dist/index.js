"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOne = exports.view = exports.update = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
const create = async (params) => {
    try {
        const schema = joi_1.default.object({
            name: joi_1.default.string().required(),
            mark: joi_1.default.string().required(),
            year: joi_1.default.date(),
            image: joi_1.default.string().uri(),
            bodega: joi_1.default.string(),
            lote: joi_1.default.number(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.create = create;
const del = async (params) => {
    try {
        const schema = joi_1.default.object({
            ids: joi_1.default.array().items(joi_1.default.number()),
            marks: joi_1.default.array().items(joi_1.default.string()),
            years: joi_1.default.array().items(joi_1.default.string()),
            bodegas: joi_1.default.array().items(joi_1.default.string()),
            lotes: joi_1.default.array().items(joi_1.default.number()),
            state: joi_1.default.boolean(),
        }).or("ids", "marks", "years", "botegas", "lotes", "state");
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.del = del;
const update = async (params) => {
    try {
        const schema = joi_1.default.object({
            id: joi_1.default.number().required(),
            name: joi_1.default.string(),
            mark: joi_1.default.string(),
            year: joi_1.default.string(),
            bodega: joi_1.default.string(),
            state: joi_1.default.boolean(),
            lote: joi_1.default.number(),
            image: joi_1.default.string().uri(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.update = update;
const view = async (params) => {
    try {
        const schema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            years: joi_1.default.string(),
            state: joi_1.default.boolean(),
            marks: joi_1.default.array().items(joi_1.default.string()),
            bodegas: joi_1.default.array().items(joi_1.default.string()),
            lotes: joi_1.default.array().items(joi_1.default.number()),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.view = view;
const findOne = async (params) => {
    try {
        const schema = joi_1.default.object({
            id: joi_1.default.number().required(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.findOne = findOne;
//# sourceMappingURL=index.js.map