export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare namespace Create {
    interface Request {
        name: string;
        mark: string;
        year?: string;
        bodega?: string;
        image?: string;
        lote?: number;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        marks?: string[];
        years?: string[];
        bodegas?: string[];
        lotes?: number[];
        state?: boolean;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        id: number;
        name?: string;
        mark?: string;
        year?: string;
        image?: string;
        bodega?: string;
        lote?: number;
        state?: boolean;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        year?: string;
        state?: boolean;
        marks?: string[];
        bodegas?: string[];
        lotes?: number[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace FindOne {
    interface Request {
        id: number;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map