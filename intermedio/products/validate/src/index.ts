import Joi from "joi"
import * as T from "./types"

export const create = async (params: T.Create.Request): Promise<T.Create.Response> => {
    
    try {

        const schema = Joi.object({

            name:  Joi.string().required(),
            mark:  Joi.string().required(),
            
            year:  Joi.date(),
            image: Joi.string().uri(),
            bodega:Joi.string(),
            lote:  Joi.number(),
        
        })

        const result = await schema.validateAsync(params)
        
        return { statusCode: "success", data: params }
    
    } catch (error) {
        
        throw { statusCode: "error", message: error.toString() }
   
    }
}

export const del = async (params: T.Delete.Request): Promise<T.Delete.Response> => {
    
    try {

        const schema = Joi.object({

            ids: Joi.array().items(Joi.number()),

            marks:  Joi.array().items(Joi.string()),
    
            years: Joi.array().items(Joi.string()),
    
            bodegas: Joi.array().items(Joi.string()),
    
            lotes: Joi.array().items(Joi.number()),
    
            state: Joi.boolean(),
    
        
        }).or("ids","marks","years","botegas","lotes","state")

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const update = async (params: T.Update.Request): Promise<T.Update .Response> => {
    
    try {

        const schema = Joi.object({

            id:      Joi.number().required(),

            name:    Joi.string(),
            mark:    Joi.string(),
            year:    Joi.string(),
            bodega:  Joi.string(),
            state:   Joi.boolean(),
            lote:    Joi.number(),
            image:   Joi.string().uri(),
        
        })

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const view = async (params: T.View.Request): Promise<T.View.Response> => {
    
    try {

        const schema = Joi.object({

            offset: Joi.number(),
            limit: Joi.number(),

            years:  Joi.string(),
            state: Joi.boolean(),

            marks:  Joi.array().items(Joi.string()),
            bodegas: Joi.array().items(Joi.string()),
            lotes: Joi.array().items(Joi.number()),
    
        })

        const result = await schema.validateAsync(params)
        

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const findOne = async (params: T.FindOne.Request): Promise<T.FindOne.Response> => {
    
    try {

        const schema = Joi.object({

            id: Joi.number().required(),

        })

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }

    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}