import { SyncDB } from "./models";

import  {run} from "./adapter";

import { redisClient  } from "./settings";

redisClient.on("error",(err)=> console.log("Redis cliet Error", err))

export { SyncDB, run }