import * as Models from  "../models"
import { InternalError } from "../settings"
import * as T from "../types"
import * as Validation from "validate-product"

export const create = async (params: T.Services.Create.Request): Promise<T.Services.Create.Response> => {
    
    try {
        
        await Validation.create(params)
        
        const { statusCode, data, message } = await Models.create(params)

        return { statusCode, data, message }
    
    } catch (error) {

        console.error({step: "Service create", error: error.toString() });
        
        return { statusCode: "error", message: InternalError}
   
    }
}

export const del = async (params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> => {
    
    try { 

        await Validation.del(params)

        let where: T.Models.Where =  { } 

        var optionals: T.Models.Attributes[] = ["id", "mark", "year", "bodega", "lote", "state"]

        if (params.state !== undefined) where.state = params.state

        for (let x of optionals.map(v=>v.concat("s"))) if(params[x] !== undefined) where[x.slice(0,-1)] = params[x]

        const { statusCode, data, message } = await Models.del({ where }) 

        if(statusCode !== "success") return { statusCode, message }

        return {statusCode: "success", data: data}
        
    } catch (error) {
     
        console.error({step:"Service delete", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const update = async (params: T.Services.Update.Request): Promise<T.Services.Update .Response> => {
    
    try {

        await Validation.update(params)

        let where: T.Models.Where =  { id: params.id} 

        const findOne = await Models.findOne({ where })

        if(findOne.statusCode !== "success"){ 

            switch (findOne.statusCode) {
                
                case "notFound": return { statusCode: "error", message:"producto no registrado"} 
            
                default: return { statusCode: "ValidationError", message: InternalError }

            }

        }

        const { statusCode, data, message } = await Models.update(params,{ where }) 

        if(statusCode !== "success") return { statusCode, message }

        return {statusCode: "success", data: data[1][0] }
        
    } catch (error) {
     
        console.error({step:"Service update", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const view = async (params: T.Services.View.Request): Promise<T.Services.View.Response> => {
    
    try {

        await Validation.view(params)

        var where: T.Models.Where =  { } 

        var optionals: T.Models.Attributes[] = ["mark","bodega","lote"]

        for (let x of optionals.map(v=>v.concat("s"))) if(params[x] !== undefined) where[x.slice(0,-1)] = params[x]
        
        
        var optionals: T.Models.Attributes[] = ["state","year"]
        
        for(let x of optionals) if(params[x]!==undefined) where[x] = params[x];

      

        const { statusCode, data, message } = await Models.findAndCountAll({ where }) 

        return { statusCode, data, message }
        
    } catch (error) {
     
        console.error({step:"Service view", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const findOne = async (params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response> => {
    
    try {

        await Validation.findOne(params)

        let where: T.Models.Where =  { id: params.id } 

        const { statusCode, data, message} = await Models.findOne({ where })

        return { statusCode, data, message} 

    } catch (error) {
     
        console.error({step:"Service findOne", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}