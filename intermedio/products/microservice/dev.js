const { SyncDB, run } = require("./dist")

const main = async () =>{    

    try {
        
        const result = await SyncDB({ force: false })

        if (result.statusCode !== "success") throw result.message

        await run()
        
    } catch (error) {
     
        console.log(error);
        
    }
}

main()