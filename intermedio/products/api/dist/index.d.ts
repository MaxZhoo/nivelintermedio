import { JobsOptions } from "bullmq";
import * as T from "./types";
export declare const name = "product";
export { T };
export declare const version: number;
export declare const Create: (props: T.Create.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Create.Response>;
export declare const Delete: (props: T.Delete.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Delete.Response>;
export declare const FindOne: (props: T.FindOne.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.FindOne.Response>;
export declare const View: (props: T.View.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.View.Response>;
export declare const Update: (props: T.Update.Request, redis: T.REDIS, opts?: JobsOptions) => Promise<T.Update.Response>;
//# sourceMappingURL=index.d.ts.map