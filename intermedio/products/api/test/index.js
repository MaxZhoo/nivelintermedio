const api =  require("../dist")

const redis = {

    local: {
        host: "192.168.19.128",
        port : 6379,
        password: undefined,
    }

}

const environmet = redis.local

const products =[
    {
           
        name: "cafe con leche",
        mark:  "supreme",
        
        year: "1960",
        bodega: "bodeguita helpme",
        lote: 107,
    
    },
    {
           
        name: "cafe sin leche",
        mark:  "supreme",
        
        year: "1960",
        bodega: "bodeguita ",
        lote: 108,
    }
]

async function create(product) {
    
    try {
        
        const result = await api.Create(product, environmet)

    

/*
        const params = {
            ids: [3,67,56,8478],
            lotes: [3454573575,90809184238499],
            marks:["etiqueta negra","don alejandro","la providencia","toro viejo"] 
        }
*/

        console.log(result);

    } catch (error) {
    
        console.log(error);
        
    }

}

async function del(years) {

    try {

        const result = await api.Delete( {years:[years]}, environmet )
        
        console.log(result);

    } catch (error) {
        
        console.log(error);

    }

}

async function update(params) {
   
    try {

        const result = await api.Update( params, environmet )

        console.log(result);
    
    } catch (error) {
   
        console.log(error);
        
    }    
}

async function view(params) {

    try {
       
        const result =  await api.View(params, environmet)  

        console.log(result.data);
       
        
    } catch (error) {
        
        console.log(error);

    }    

}

async function findOne(username) {

    try {

        const result = await api.FindOne({username}, environmet)

    } catch (error) {
        
        console.log(error);

    }

}

const main = async() =>{
    
    try {
    
        await create(products[0])

        //await view()//trae todo
        //await view({lotes: [104]}) //trae todos los de ese lote
        //await view({lotes: [104,105]}) //trae todos los de ese rango de lotes

        //await del("2022")
        
    } catch (error) {
        
        console.log(error);

    }
} 
main()