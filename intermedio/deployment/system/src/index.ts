import * as user from "users"
import * as product from "product"
import * as shopp from "shopp"

const main = async()=>{
    try {
        
        await user.SyncDB({force: false});
        await product.SyncDB({force: false});
        await shopp.SyncDB({force: false});

        user.run();
        product.run();
        shopp.run();

    } catch (error) {
        
        console.log(error);
        
    }

}

main()