import Joi from "joi"
import * as T from "./types"

export const create = async (params: T.Create.Request): Promise<T.Create.Response> => {
    
    try {

        const schema = Joi.object({

            username: Joi.string().required(),
            fullName: Joi.string().required(),
            phone:    Joi.number().required(),
            image:    Joi.string().uri().required(),
        
        })

        const result = await schema.validateAsync(params)

        if(params.username==="root"||"admin"||"su"){

            throw { statusCode: "error", message: "Nombre reservado para su uso interno"};
            
        }
        
        return { statusCode: "success", data: params }
    
    } catch (error) {
        
        throw { statusCode: "error", message: error.toString() }
   
    }
}

export const del = async (params: T.Delete.Request): Promise<T.Delete.Response> => {
    
    try {

        const schema = Joi.object({

            username: Joi.string().required(),
        
        })

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const update = async (params: T.Update.Request): Promise<T.Update .Response> => {
    
    try {

        const schema = Joi.object({

            username: Joi.string().required(),
            fullName: Joi.string(),
            phone:    Joi.number(),
            image:    Joi.string().uri(),
        
        })

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const view = async (params: T.View.Request): Promise<T.View.Response> => {
    
    try {

        const schema = Joi.object({

            offset: Joi.number(),
            limit: Joi.number(),
            state: Joi.boolean(),
           
        
        })

        const result = await schema.validateAsync(params)


        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const findOne = async (params: T.FindOne.Request): Promise<T.FindOne.Response> => {
    
    try {

        const schema = Joi.object({

            username: Joi.string().required(),
 
        })

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }

    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}
