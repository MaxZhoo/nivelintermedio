"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOne = exports.view = exports.update = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
const create = async (params) => {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
            fullName: joi_1.default.string().required(),
            phone: joi_1.default.number().required(),
            image: joi_1.default.string().uri().required(),
        });
        const result = await schema.validateAsync(params);
        if (params.username === "root" || "admin" || "su") {
            throw { statusCode: "error", message: "Nombre reservado para su uso interno" };
        }
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.create = create;
const del = async (params) => {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.del = del;
const update = async (params) => {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
            fullName: joi_1.default.string(),
            phone: joi_1.default.number(),
            image: joi_1.default.string().uri(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.update = update;
const view = async (params) => {
    try {
        const schema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            state: joi_1.default.boolean(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.view = view;
const findOne = async (params) => {
    try {
        const schema = joi_1.default.object({
            username: joi_1.default.string().required(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.findOne = findOne;
//# sourceMappingURL=index.js.map