export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare namespace Create {
    interface Request {
        username: string;
        fullName: string;
        phone: number;
        image: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        username: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        username: string;
        fullName?: string;
        phone?: number;
        image?: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace FindOne {
    interface Request {
        username: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map