import * as T from "./types";
export declare const create: (params: T.Create.Request) => Promise<T.Create.Response>;
export declare const del: (params: T.Delete.Request) => Promise<T.Delete.Response>;
export declare const update: (params: T.Update.Request) => Promise<T.Update.Response>;
export declare const view: (params: T.View.Request) => Promise<T.View.Response>;
export declare const findOne: (params: T.FindOne.Request) => Promise<T.FindOne.Response>;
//# sourceMappingURL=index.d.ts.map