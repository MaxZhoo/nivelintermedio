const api =  require("../dist")

const redis = {

    local: {
        host: "192.168.19.128",
        port : 6379,
        password: undefined,
    }

}

const environmet = redis.local

const users =[
    {
           
        fullName:"juanito HS",
        image: "https//............",
        phone: "861234567893",
        username: "asdasdasdasd"
    
    },
    {
           
        fullName:"Pepito HS",
        image: "https//............",
        phone: "8621465989173",
        username: "xdxdxdxdxdxdxdxd"
    
    }
]

async function create(users) {
    
    try {
        
        const result = await api.Create(users, environmet)

        console.log(result);

    } catch (error) {
    
        console.log(error);
        
    }

}

async function del(username) {

    try {

        const result = await api.Delete( { username }, environmet )
        
        console.log(result);

    } catch (error) {
        
        console.log(error);

    }

}

async function update(params) {
   
    try {

        const result = await api.Update( params, environmet )

        console.log(result);
    
    } catch (error) {
   
        console.log(error);
        
    }    
}

async function view(params) {

    try {

        const result =  await api.View(params, environmet)

        console.log(result);
        
    } catch (error) {
        
        console.log(error);

    }    

}

async function findOne(username) {

    try {

        const result = await api.FindOne({username}, environmet)

    } catch (error) {
        
        console.log(error);

    }

}

const main = async() =>{
    
    try {
    
        await view()

        //await create(users[0])

        await del(users[0].username)

        await view()
        
    } catch (error) {
        
        console.log(error);

    }
} 
main()