"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Update = exports.View = exports.FindOne = exports.Delete = exports.Create = exports.version = exports.name = void 0;
const bullmq_1 = require("bullmq");
exports.name = "users";
exports.version = 2;
const Create = async (props, redis, opts) => {
    try {
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = "create";
        const job = await queue.add(endpoint, props, opts);
        await job.waitUntilFinished(queueEvents);
        const result = await bullmq_1.Job.fromId(queue, job.id);
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.Create = Create;
const Delete = async (props, redis, opts) => {
    try {
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = "delete";
        const job = await queue.add(endpoint, props, opts);
        await job.waitUntilFinished(queueEvents);
        const result = await bullmq_1.Job.fromId(queue, job.id);
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.Delete = Delete;
const FindOne = async (props, redis, opts) => {
    try {
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = "findOne";
        const job = await queue.add(endpoint, props, opts);
        await job.waitUntilFinished(queueEvents);
        const result = await bullmq_1.Job.fromId(queue, job.id);
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.FindOne = FindOne;
const View = async (props, redis, opts) => {
    try {
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = "view";
        const job = await queue.add(endpoint, props, opts);
        await job.waitUntilFinished(queueEvents);
        const result = await bullmq_1.Job.fromId(queue, job.id);
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.View = View;
const Update = async (props, redis, opts) => {
    try {
        const queue = new bullmq_1.Queue(`${exports.name}:${exports.version}`, { connection: redis });
        const queueEvents = new bullmq_1.QueueEvents(`${exports.name}:${exports.version}`, { connection: redis });
        const endpoint = "update";
        const job = await queue.add(endpoint, props, opts);
        await job.waitUntilFinished(queueEvents);
        const result = await bullmq_1.Job.fromId(queue, job.id);
        const { statusCode, data, message } = result.returnvalue;
        return { statusCode, data, message };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.Update = Update;
//# sourceMappingURL=index.js.map