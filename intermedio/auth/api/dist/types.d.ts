export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare const endpoint: readonly ["create", "update", "delete", "findOne", "view"];
export declare type Endpoint = typeof endpoint[number];
export interface REDIS {
    host: string;
    port: number;
    password: string;
}
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export interface Model {
    id?: number;
    username?: string;
    password?: string;
    fullName?: string;
    image?: string;
    state?: boolean;
    phone?: number;
    createdAt?: string;
    updatedAt?: string;
}
export declare namespace Create {
    interface Request {
        username: string;
        fullName: string;
        phone: number;
        image: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        username: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Update {
    interface Request {
        username: string;
        fullName?: string;
        phone?: number;
        image?: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        state?: boolean;
    }
    interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }
}
export declare namespace FindOne {
    interface Request {
        username: string;
    }
    interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map