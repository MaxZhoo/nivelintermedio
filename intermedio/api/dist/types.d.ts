export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare namespace Users {
    interface Model {
        id: number;
        username: string;
        password: string;
        fullName: string;
        image: string;
        state: boolean;
        phone: number;
        createdAt: string;
        updatedAt: string;
    }
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    namespace Create {
        interface Request {
            username: string;
            fullName: string;
            phone: number;
            image: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            username: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            username: string;
            fullName?: string;
            phone?: number;
            image?: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            state?: boolean;
        }
        interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Request {
            username?: string;
            id?: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
}
export declare namespace Product {
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    interface Model {
        id: number;
        name: string;
        mark: string;
        year: string;
        bodega: string;
        state: boolean;
        lote: number;
        image: string;
        createdAt: string;
        updatedAt: string;
    }
    namespace Create {
        interface Request {
            name: string;
            mark: string;
            year?: string;
            bodega?: string;
            image?: string;
            lote?: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            marks?: string[];
            years?: string[];
            bodegas?: string[];
            lotes?: number[];
            state?: boolean;
        }
        interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace Update {
        interface Request {
            id: number;
            name?: string;
            mark?: string;
            year?: string;
            image?: string;
            bodega?: string;
            lote?: number;
            state?: boolean;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            year?: string;
            state?: boolean;
            marks?: string[];
            bodegas?: string[];
            lotes?: number[];
        }
        interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
    namespace FindOne {
        interface Request {
            id: number;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
}
export declare namespace Shopp {
    interface Paginate {
        data: Model[];
        itemCount: number;
        pageCount: number;
    }
    interface Model {
        id: number;
        user: string;
        product: string;
        createdAt: string;
        updatedAt: string;
    }
    namespace Create {
        interface Request {
            user: string;
            product: string;
        }
        interface Response {
            statusCode: statusCode;
            data?: Model;
            message?: string;
        }
    }
    namespace Delete {
        interface Request {
            ids?: number[];
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: statusCode;
            data?: number;
            message?: string;
        }
    }
    namespace View {
        interface Request {
            offset?: number;
            limit?: number;
            users?: string[];
            products?: string[];
        }
        interface Response {
            statusCode: statusCode;
            data?: Paginate;
            message?: string;
        }
    }
}
//# sourceMappingURL=types.d.ts.map