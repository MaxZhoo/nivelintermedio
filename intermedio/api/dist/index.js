"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const socket_io_client_1 = require("socket.io-client");
const socket = (0, socket_io_client_1.io)("http://localhost");
const main = async () => {
    try {
        setTimeout(() => console.log(socket.id), 1500);
        socket.on("res:user:view", ({ statusCode, data, message }) => {
            const result = { statusCode, data, message };
            console.log("res:user:view", result.statusCode, result.data);
        });
        socket.on("connect_error", (err) => {
            console.log(err.message);
        });
        setTimeout(() => {
            socket.emit("req:user:view", ({}));
        }, 3000);
    }
    catch (error) {
        console.log(error);
    }
};
main();
//# sourceMappingURL=index.js.map