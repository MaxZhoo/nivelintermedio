export type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError"

export namespace Users {

    export interface Model {

        id : number;
    
        username : string,
    
        password : string;
    
        fullName : string;
        
        image : string;
        
        state : boolean;
    
        phone : number;
    
        createdAt : string;
    
        updatedAt : string;
    
    }

    export interface Paginate {
        data: Model [],
        itemCount: number,
        pageCount: number,
    }

    export namespace Create{

        export interface Request{
            username: string;
            fullName: string;
            phone:    number;
            image:    string;
        }
        
        export interface Response{
            
            statusCode: statusCode;
            data  ?:  Model;
            message ?:    string;
        
        }

    }

    export namespace Delete{
    
        export interface Request{
            username: string;
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Model  ;
            message ?:    string;
        }
    }

    export namespace Update{
    
        export interface Request{

            username : string;
            fullName?: string;
            phone?:    number;
            image?:    string;
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Model;
            message ?: string;
        }
    }

    export namespace View{
    
        export interface Request{
            offset?: number;
            limit?: number;
            state?: boolean;
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Paginate;
            message ?: string;
        }
    }

    export namespace FindOne{
    
        export interface Request{
            username?: string;
            id?: number;
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Model;
            message ?:    string;
        }
    }
    
}

export namespace Product {
    export interface Paginate {
        data: Model [],
        itemCount: number,
        pageCount: number,
    }
    
    export interface Model {
    
        id: number;
    
        name: string,
    
        mark:  string,
    
        year: string,
    
        bodega: string,
    
        state: boolean,
    
        lote: number,
    
        image: string,
    
        createdAt: string;
    
        updatedAt: string;
    
    } 
    export namespace Create{
    
        export interface Request{
    
            name: string,
            mark:  string,
            
            year?: string,
            bodega?: string,
            image?: string,
            lote?: number,
    
        }
        
        export interface Response{
            
            statusCode: statusCode;
            data  ?:  Model ;
            message ?:    string;
        
        }
    
    }
    
    export namespace Delete{
    
        export interface Request{
    
            ids?: number[],
    
            marks?:  string[],
    
            years?: string[],
    
            bodegas?: string[],
    
            lotes?: number[],
    
            state?: boolean,
    
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: number;
            message ?:    string;
        }
    }
    
    export namespace Update{
    
        export interface Request{
    
            id: number;
    
            name?: string,
            mark?:  string,
            year?: string,
            image?: string,
            bodega?: string,
            lote?: number,
            state?: boolean,
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Model;
            message ?:    string;
        }
    }
    
    export namespace View{
    
        export interface Request{
            offset?: number;
            limit?: number;
    
            year?: string,
            state?: boolean;
    
            marks?: string[],
            bodegas?:string[],
            lotes?:  number[],
    
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Paginate;
            message ?:    string;
        }
    }
    
    export namespace FindOne{
    
        export interface Request{
           id: number;
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Model ;
            message ?:    string;
        }
    }
}

export namespace Shopp {

    export interface Paginate {
        data: Model [],
        itemCount: number,
        pageCount: number,
    }
    
    export interface Model {
    
        id: number;
    
        user: string,
    
        product:  string,
    
        createdAt: string;
    
        updatedAt: string;
    
    } 
    export namespace Create{
    
        export interface Request{
    
            user: string,
            product:  string,
    
        }
        
        export interface Response{
            
            statusCode: statusCode;
            data  ?:  Model;
            message ?:    string;
        
        }
    
    }
    
    export namespace Delete{
    
        export interface Request{
    
            ids?: number[],
    
            users?: string[],
            products?:  string[],
    
        }
        
        export interface Response{
            statusCode  : statusCode;
            data?       : number;
            message?    :    string;
        }
    }
    
    export namespace View{
    
        export interface Request{
            offset?: number;
            limit?: number;
    
            users?: string[],
            products?:  string[],
    
        }
        
        export interface Response{
            statusCode: statusCode;
            data  ?: Paginate;
            message ?:    string;
        }
    }
    
}