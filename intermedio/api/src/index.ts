import { io } from "socket.io-client";
const socket = io("http://localhost");

const main = async () => {
    
    try {
        
        setTimeout(()=>console.log(socket.id), 1500);

        socket.on("res:user:view",({statusCode, data, message})=>{

            const result = {statusCode, data, message}

            console.log("res:user:view",result.statusCode,result.data);
            
        })
        
        socket.on("connect_error",(err)=>{
            
            console.log(err.message);
            
        })


        setTimeout(()=>{

            socket.emit("req:user:view",({ }))

        },3000)


    } catch (error) {

        console.log(error);
    
    }

}

main()