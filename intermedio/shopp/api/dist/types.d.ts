export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare const endpoint: readonly ["create", "delete", "view"];
export declare type Endpoint = typeof endpoint[number];
export interface REDIS {
    host: string;
    port: number;
    password: string;
}
export interface Paginate {
    data: Model[];
    itemCount: number;
    pageCount: number;
}
export interface Model {
    id: number;
    user: number;
    product: number;
    createdAt: string;
    updatedAt: string;
}
export declare namespace Create {
    interface Request {
        user: number;
        product: number;
    }
    interface Response {
        statusCode: statusCode;
        data?: Model;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: number[];
        products?: number[];
    }
    interface Response {
        statusCode: statusCode;
        data?: number;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: number[];
        products?: number[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Paginate;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map