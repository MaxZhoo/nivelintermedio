export type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError"

export const endpoint = ["create","delete","view"] as const

export type Endpoint = typeof endpoint[number]

 
export interface REDIS {
    
    host: string,
    port: number,
    password: string,

}

export interface Paginate {
    data: Model [],
    itemCount: number,
    pageCount: number,
}

export interface Model {

    id: number;

    user: number,

    product:  number,

    createdAt: string;

    updatedAt: string;

} 
export namespace Create{

    export interface Request{

        user: number,
        product:  number,

    }
    
    export interface Response{
        
        statusCode: statusCode;
        data  ?:  Model;
        message ?:    string;
    
    }

}

export namespace Delete{

    export interface Request{

        ids?: number[],

        users?: number[],
        products?:  number[],

    }
    
    export interface Response{
        statusCode  : statusCode;
        data?       : number;
        message?    :    string;
    }
}

export namespace View{

    export interface Request{
        offset?: number;
        limit?: number;

        users?: number[],
        products?:  number[],

    }
    
    export interface Response{
        statusCode: statusCode;
        data  ?: Paginate;
        message ?:    string;
    }
}
