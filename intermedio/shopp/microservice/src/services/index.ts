import * as Models from  "../models"
import { InternalError } from "../settings"
import * as T from "../types"
import * as Controllers from "../controller"
import * as Validation from "validate-shopp"

export const create = async (params: T.Services.Create.Request): Promise<T.Services.Create.Response> => {
    
    try {
        
        await Validation.create(params)
 
        //validacion del usuario
        const user = (await Controllers.ValidateUser({ user: params.user})).data

        console.log(user);
        

        if(!user.state) throw {statusCode: "ValidationError",message:"El usuario esta inhabilitado"}

        //validacion del producto
        const product = (await Controllers.ValidateProduct({ product: params.product})).data

        console.log(product);
        
        if(!product.state) throw {statusCode: "ValidationError",message:"El producto no hay stock para realizar ventas"}


        const { statusCode, data, message } = await Models.create(params)

        return { statusCode, data, message }
    
    } catch (error) {

        console.error({step: "Service create", error: error.toString() });
        
        return { statusCode: "error", message: InternalError}
   
    }
}

export const del = async (params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> => {
    
    try { 

        await Validation.del(params)

        

        let where: T.Models.Where =  { } 

        var optionals: T.Models.Attributes[] = ["id", "product", "user"]

        for (let x of optionals.map(v=>v.concat("s"))) if(params[x] !== undefined) where[x.slice(0,-1)] = params[x]

        const { statusCode, data, message } = await Models.del({ where }) 

        if(statusCode !== "success") return { statusCode, message }

        return {statusCode: "success", data: data}
        
    } catch (error) {
     
        console.error({step:"Service delete", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const view = async (params: T.Services.View.Request): Promise<T.Services.View.Response> => {
    
    try {

        await Validation.view(params)

        var where: T.Models.Where =  { } 

        var optionals: T.Models.Attributes[] = ["user","product"]

        for (let x of optionals.map(v=>v.concat("s"))) if(params[x] !== undefined) where[x.slice(0,-1)] = params[x]

        const { statusCode, data, message } = await Models.findAndCountAll({ where }) 

        return { statusCode, data, message }
        
    } catch (error) {
     
        console.error({step:"Service view", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}