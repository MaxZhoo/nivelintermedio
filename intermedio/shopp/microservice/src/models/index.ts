import * as S from "sequelize";

import { sequelize, name } from "../settings";

import { Models as T } from "../types";

export const Model = sequelize.define<T.Model>(name, {

	user: { type: S.DataTypes.BIGINT },

	product:  { type: S.DataTypes.BIGINT},

}, { freezeTableName: true });
 
export const count = async (options?: T.count.Request): Promise<T.count.Response> => {
	
	try {

		const instance: number = await Model.count(options)

		return {statusCode: "success", data: instance };

	} catch (error) {

		console.log({step : "Models count", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}

}

export const create = async (values: T.create.Request, options?: T.create.Opts ): Promise<T.create.Response> => {
	
	try {
		
		const instance: T.Model = await Model.create(values, options)

		return { statusCode: "success", data: instance.toJSON() }

	} catch (error) {

		console.log({step : "Models create", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}
}

export const del = async (options?: T.Delete.Opts): Promise<T.Delete.Response> => {
	
	try {
		
		const instance: number = await Model.destroy(options)

		return {statusCode: "success", data: instance}

	} catch (error) {

		console.log({step : "Models del", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}
}

export const findAndCountAll = async (options?: T.FindAndCountAll.Opts): Promise<T.FindAndCountAll.Response> => {
	
	try {

		var options: T.FindAndCountAll.Opts = { ...{limit: 12, offset:0}, ...options };

		const { count, rows}: { rows: T.Model[]; count: number; } = await Model.findAndCountAll()
		
		return {
			statusCode: "success", 
			data: {
				data: rows.map(v => v.toJSON() ),
				itemCount: count,
				pageCount: Math.ceil( count / options.limit ),
			}
		}
		
	} catch (error) {

		console.log({step : "Models view", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}
}

export const findOne = async (options?: T.FindOne.Opts): Promise<T.FindOne.Response> => {
	
	try {

		const instance: T.Model | null = await Model.findOne(options)
		
		if(instance) return {statusCode: "success" , data: instance.toJSON() }

		else return {statusCode: "notFound", message: "not found"}

	} catch (error) {

		console.log({step : "Models findOne", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}
}

export const update = async (values?: T.Update.Request ,options?: T.Update.Opts): Promise<T.Update.Response> => {
	
	try {

		var options: T.Update.Opts = { ...{ returning: true }, ...options }

		const instances: [number, T.Model[]] = await Model.update(values, options)

		return { statusCode: "success", data:[ instances[0], instances[1].map(v => v.toJSON() ) ] }

	} catch (error) {

		console.log({step : "Models update", error: error.toString() });
		
		return { statusCode: "error", message: error.toString() }
	
	}

}

export const SyncDB = async (params: T.SyncDb.Request): Promise< T.SyncDb.Response > => {

	try {
		
		const model: T.Model  = await Model.sync(params)
		
		return  { statusCode: "success" , data: model } 

	} catch (error) {
		
		console.log( {step : "Models  SyncDB", error: error.toString() });
		
		return { statusCode: "error" , message: error.toString() }
	
	}

}
 