export type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError"

export namespace Create{

    export interface Request{

        user: number,
        product:  number,

    }
    
    export interface Response{
        
        statusCode: statusCode;
        data  ?:  Request;
        message ?:    string;
    
    }

}

export namespace Delete{

    export interface Request{

        ids?: number[],
    
        users?: number[],
        products?:  number[],

    }
    
    export interface Response{
        statusCode: statusCode;
        data  ?: Request ;
        message ?:    string;
    }
}


export namespace View{

    export interface Request{
        offset?: number;
        limit?: number;

        users?: number[],
        products?:  number[],

    }
    
    export interface Response{
        statusCode: statusCode;
        data  ?: Request;
        message ?:    string;
    }
}
