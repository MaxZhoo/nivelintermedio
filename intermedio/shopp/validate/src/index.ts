import Joi from "joi"
import * as T from "./types"

export const create = async (params: T.Create.Request): Promise<T.Create.Response> => {
    
    try {

        const schema = Joi.object({

            user:  Joi.number().required(),
            product:  Joi.number().required(),
        
        })

        const result = await schema.validateAsync(params)
        
        return { statusCode: "success", data: params }
    
    } catch (error) {
        
        throw { statusCode: "error", message: error.toString() }
   
    }
}

export const del = async (params: T.Delete.Request): Promise<T.Delete.Response> => {
    
    try {

        const schema = Joi.object({

            ids: Joi.array().items(Joi.number()),

            users:  Joi.array().items(Joi.number()),
    
            products: Joi.array().items(Joi.number()),
        
        }).xor("ids","users","products")

        const result = await schema.validateAsync(params)

        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}

export const view = async (params: T.View.Request): Promise<T.View.Response> => {
    
    try {

        const schema = Joi.object({

            offset: Joi.number(),
            limit: Joi.number(),
            
            users:  Joi.array().items(Joi.number()),
    
            products: Joi.array().items(Joi.number()),
    
        })

        const result = await schema.validateAsync(params)


        return { statusCode: "success", data: params }
        
    } catch (error) {
     
        throw { statusCode: "error", message: error.toString() }

    }
}