export declare type statusCode = "success" | "error" | "notFound" | "notPermitted" | "ValidationError";
export declare namespace Create {
    interface Request {
        user: number;
        product: number;
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace Delete {
    interface Request {
        ids?: number[];
        users?: number[];
        products?: number[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
export declare namespace View {
    interface Request {
        offset?: number;
        limit?: number;
        users?: number[];
        products?: number[];
    }
    interface Response {
        statusCode: statusCode;
        data?: Request;
        message?: string;
    }
}
//# sourceMappingURL=types.d.ts.map