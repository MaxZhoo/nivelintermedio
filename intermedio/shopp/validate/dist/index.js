"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.view = exports.del = exports.create = void 0;
const joi_1 = __importDefault(require("joi"));
const create = async (params) => {
    try {
        const schema = joi_1.default.object({
            user: joi_1.default.number().required(),
            product: joi_1.default.number().required(),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.create = create;
const del = async (params) => {
    try {
        const schema = joi_1.default.object({
            ids: joi_1.default.array().items(joi_1.default.number()),
            users: joi_1.default.array().items(joi_1.default.number()),
            products: joi_1.default.array().items(joi_1.default.number()),
        }).xor("ids", "users", "products");
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.del = del;
const view = async (params) => {
    try {
        const schema = joi_1.default.object({
            offset: joi_1.default.number(),
            limit: joi_1.default.number(),
            users: joi_1.default.array().items(joi_1.default.number()),
            products: joi_1.default.array().items(joi_1.default.number()),
        });
        const result = await schema.validateAsync(params);
        return { statusCode: "success", data: params };
    }
    catch (error) {
        throw { statusCode: "error", message: error.toString() };
    }
};
exports.view = view;
//# sourceMappingURL=index.js.map