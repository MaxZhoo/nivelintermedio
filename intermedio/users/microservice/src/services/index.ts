import * as Models from  "../models"
import { InternalError } from "../settings"
import * as T from "../types"
import * as Validation from "validate-user"

export const create = async (params: T.Services.Create.Request): Promise<T.Services.Create.Response> => {
    
    try {
        
        await Validation.create(params)
        
        const findOne = await Models.findOne({ where: { username: params.username } })
        

        if(findOne.statusCode !== "notFound"){ 

            switch (findOne.statusCode) {
                
                case "success": return { statusCode: "error", message:"Usuario ya registrado"} 
            
                default: return { statusCode: "ValidationError", message: InternalError }

            }

        }
    
        const { statusCode, data, message } = await Models.create(params)

        return { statusCode, data, message }
    
    } catch (error) {

        console.error({step: "Service create", error: error.toString() });
        
        return { statusCode: "error", message: InternalError}
   
    }
}

export const del = async (params: T.Services.Delete.Request): Promise<T.Services.Delete.Response> => {
    
    try {

        await Validation.del(params)

        let where: T.Models.Where =  { username: params.username } 

        const findOne = await Models.findOne({ where })

        if(findOne.statusCode !== "success"){ 

            switch (findOne.statusCode) {
                
                case "notFound": return { statusCode: "error", message:"No existe el usuario a eliminar"} 
            
                default: return { statusCode: "ValidationError", message: InternalError }

            }

        }

        const { statusCode, message } = await Models.del({ where }) 

        if(statusCode !== "success") return { statusCode, message }

        return {statusCode: "success", data: findOne.data}
        
    } catch (error) {
     
        console.error({step:"Service delete", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const update = async (params: T.Services.Update.Request): Promise<T.Services.Update .Response> => {
    
    try {

        await Validation.update(params)

        let where: T.Models.Where =  { username: params.username } 

        const findOne = await Models.findOne({ where })

        if(findOne.statusCode !== "success"){ 

            switch (findOne.statusCode) {
                
                case "notFound": return { statusCode: "error", message:"No existe el usuario a actualizar"} 
            
                default: return { statusCode: "ValidationError", message: InternalError }

            }

        }

        if(!findOne.data.state){

            return {statusCode: "notPermitted", message: "Tu usuario no está habilitado"}

        }

        const { statusCode, data, message } = await Models.update(params,{ where }) 

        if(statusCode !== "success") return { statusCode, message }

        return {statusCode: "success", data: data[1][0] }
        
    } catch (error) {
     
        console.error({step:"Service update", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const view = async (params: T.Services.View.Request): Promise<T.Services.View.Response> => {
    
    try {

        await Validation.view(params)

        let where: T.Models.Where = {}; 

        let optionals :  T.Models.Attributes[] = ["state"]

        for (let x of optionals) if (params[x] !== undefined) where[x] = params[x];

        const { statusCode, data, message } = await Models.findAndCountAll({ where }) 

        return { statusCode, data, message }
        
    } catch (error) {
     
        console.error({step:"Service view", error: error.toString() });

        return { statusCode: "error", message: InternalError}

    }
}

export const findOne = async (params: T.Services.FindOne.Request): Promise<T.Services.FindOne.Response> => {
    
    try {
        
        await Validation.findOne(params)

        let where: T.Models.Where =  {}

        var optionals: T.Models.Attributes[] = ["id","username"];
        
        for( let x of optionals ) if( params[x] !== undefined ) where[x] = params[x]
        
        
        const { statusCode, data, message} = await Models.findOne({ where })
        
        return { statusCode, data, message} 

    } catch (error) {
     
        console.error({step:"Service findOne", error: error });

        return { statusCode: "error", message: InternalError}

    }
}